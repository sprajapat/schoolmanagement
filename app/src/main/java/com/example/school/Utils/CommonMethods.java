package com.example.school.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class CommonMethods {

    public static boolean isConnected(Context context) {
        Boolean flag = false;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mMobileNewtork = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mWifi.isConnected() || mMobileNewtork.isConnected()) {
            // Do whatever
            //Toast.makeText(getApplicationContext(), "Connected ",Toast.LENGTH_SHORT).show();

            flag = true;
        }

        return flag;

    }

    public static class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static StringBuilder queryForStudent(String rollno) {
        StringBuilder sb = new StringBuilder();
        sb.append("/?id=").append(rollno);
        return sb;
    }

}
