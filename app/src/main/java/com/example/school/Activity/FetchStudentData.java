package com.example.school.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.school.R;
import com.example.school.Utils.CommonConstants;
import com.example.school.Utils.CommonMethods;
import com.example.school.Utils.VolleySingleton;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Objects;


public class FetchStudentData extends AppCompatActivity {
    TextView txtName, txtEmail, txtGender, txtContact, txtDOB, txtAddress;
    Button btnSubmit;
    EditText edtRollNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_student_data);
        findViewById();
        init();
    }

    private void findViewById() {
        edtRollNo = findViewById(R.id.edtRollNo);
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        txtGender = findViewById(R.id.txtGender);
        txtContact = findViewById(R.id.txtContact);
        txtDOB = findViewById(R.id.txtDOB);
        txtAddress = findViewById(R.id.txtAddress);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    private void init() {

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonMethods.isConnected(Objects.requireNonNull(getApplicationContext()))) {
                    String roll_no = edtRollNo.getText().toString();
                    StringBuilder sb = CommonMethods.queryForStudent(roll_no);

             /*       JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                            CommonConstants.BASE_URL+sb, null, response -> {
                        try {
                            Log.e("total items", String.valueOf(response));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                        error.printStackTrace();
                        CommonMethods.showToast(getApplicationContext(), String.valueOf(error));
                      });*/

                    /*JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, CommonConstants.BASE_URL,null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response >> ", response.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("error", error.toString());
                        }
                    });*/

                    StringRequest stringRequest = new StringRequest(Request.Method.GET, CommonConstants.BASE_URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("response >> ", response);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("error", error.toString());
                        }
                    }
                    );
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(6000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    VolleySingleton volleySingleton = VolleySingleton.getInstance(getApplicationContext());
                    volleySingleton.addToRequestQueue(stringRequest);
                } else {
                    CommonMethods.showToast(getApplicationContext(), getResources().getString(R.string.Internet_connectivity));
                }
            }
        });
    }
}