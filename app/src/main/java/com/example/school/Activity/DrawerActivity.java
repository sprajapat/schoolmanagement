package com.example.school.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.school.Fragment.DashboardFragment;
import com.example.school.R;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

public class DrawerActivity extends AppCompatActivity {
    public SlidingRootNav slidingRootNav;
    Fragment currentFragment;
    LinearLayout llContainer;
    ImageView imgDashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        init(savedInstanceState);
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withDragDistance(150) //Horizontal translation of a view. Default == 180dp
                .withRootViewScale(0.7f) //Content view's scale will be interpolated between 1f and 0.7f. Default == 0.65f;
                .withRootViewElevation(10) //Content view's elevation will be interpolated between 0 and 10dp. Default == 8.
                .withRootViewYTranslation(3)
                .withMenuLayout(R.layout.drawer_nav_menu)
                .inject();
    }

    private void init(Bundle savedInstanceState) {
        llContainer = findViewById(R.id.llContainer);
        imgDashboard = findViewById(R.id.imgDashboard);

        imgDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.openMenu();
            }
        });
        DashboardFragment dashboardFragment = new DashboardFragment();
        changeFragment(dashboardFragment);
    }

    public void changeFragment(Fragment targetFragment) {
        currentFragment = targetFragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.llContainer, targetFragment)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}