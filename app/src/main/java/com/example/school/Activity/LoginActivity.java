package com.example.school.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.school.R;

public class LoginActivity extends AppCompatActivity {
    public boolean txtAdminisSelected;
    Button btnLogin;
    EditText edtUniqueId, edtUser, edtPass;
    TextView txtAdmin, txtSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById();
        init();
    }

    private void findViewById() {
        btnLogin = findViewById(R.id.btnLogin);
        edtUniqueId = findViewById(R.id.edtUniqueId);
        txtAdmin = findViewById(R.id.txtAdmin);
    }

    private void init() {
        txtAdminisSelected = false;
        txtAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txtAdminisSelected) {
                    edtUniqueId.setVisibility(View.VISIBLE);
                    txtAdmin.setText("Login as Parent?");
                    txtAdminisSelected = true;
                } else {
                    edtUniqueId.setVisibility(View.GONE);
                    txtAdmin.setText("Login as Admin?");
                    txtAdminisSelected = false;
                }
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DrawerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}