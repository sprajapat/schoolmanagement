package com.example.school.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.school.R;
import com.example.school.Utils.CommonConstants;
import com.example.school.Utils.CommonMethods;
import com.example.school.Utils.VolleySingleton;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddStudentData extends AppCompatActivity {
    EditText edtRollNo, edtName, edtGender, edtContact, edtEmail, edtDOB, edtAddress;
    ImageView imgView;
    Button btnSubmit;
    private final static int IMAGE_PICK = 1;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_data);
        findViewById();
        init();
    }

    private void findViewById() {
        edtRollNo = findViewById(R.id.edtRollNo);
        edtName = findViewById(R.id.edtName);
        edtEmail = findViewById(R.id.edtEmail);
        edtGender = findViewById(R.id.edtGender);
        edtContact = findViewById(R.id.edtContact);
        edtDOB = findViewById(R.id.edtDOB);
        edtAddress = findViewById(R.id.edtAddress);
        btnSubmit = findViewById(R.id.btnSubmit);
        imgView = findViewById(R.id.imgView);
    }

    private void init() {
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chooseFile = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                chooseFile.setType("image/*");
                chooseFile = Intent.createChooser(chooseFile, "Choose a image");
                startActivityForResult(chooseFile, IMAGE_PICK);
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonMethods.isConnected(Objects.requireNonNull(getApplicationContext()))) {
                    String roll_no = edtRollNo.getText().toString();
                    StringBuilder sb = CommonMethods.queryForStudent(roll_no);

                    String photoname = edtName.getText().toString() + ".jpeg";
                    final Map<String, String> stringMap = new HashMap<>();
                    stringMap.put("name", edtName.getText().toString());
                    stringMap.put("email", edtEmail.getText().toString());
                    stringMap.put("gender", edtGender.getText().toString());
                    stringMap.put("contact", edtContact.getText().toString());
                    stringMap.put("dob", edtDOB.getText().toString());
                    stringMap.put("address", edtAddress.getText().toString());
                    stringMap.put("photoname", photoname);
                    stringMap.put("photo", imageToString(bitmap));

                    final JSONObject jString = new JSONObject(stringMap);

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                            CommonConstants.BASE_URL, jString, response -> {
                        try {
                            Log.e("response", String.valueOf(response));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                        error.printStackTrace();
                        CommonMethods.showToast(getApplicationContext(), String.valueOf(error));
                    });

                    VolleySingleton volleySingleton = VolleySingleton.getInstance(getApplicationContext());
                    volleySingleton.addToRequestQueue(jsonObjectRequest);
                } else {
                    CommonMethods.showToast(getApplicationContext(), getResources().getString(R.string.Internet_connectivity));
                }


            }
        });

    }

    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_PICK && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("error", e.toString());
            }
            imgView.setImageURI(selectedImage);
        } else {
            Log.e("msg", "Sorry for img");
        }

    }
}